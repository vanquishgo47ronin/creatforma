using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using TMPro;

public class MainCharester : MonoBehaviour
{

    [SerializeField] private FormaVie _forma;
    [SerializeField] private GameObject _obj;

    //������� - ��� ����� ��� ����.
    //public delegate void Action();

    // STATIC - ����� �������� ������ � �������  // ��������� ���������
    public static event Action/*<string, int, string, string>*/ OnFormaEvent; // event - �������

    //string _index = _clas.value;

    private void Awake()
    {
        _obj.SetActive(false);
    }

    private void Update()
    {
        
        if (Input.GetKey(KeyCode.E))
        {
            _obj.SetActive(true);
        }
        else if (Input.GetKeyDown(KeyCode.Return))
        {
            // ( ? ) - �� �����
            OnFormaEvent?.Invoke(); // ����� ������� ��������� �� ������ ������ - ����������������� ����� �������� �������� ���� �������(?).Invoke().
            //_forma.UbgrateFiles();
        }
    }

    public void OffForm()
    {
        _obj.SetActive(false);
    }
}
