using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class FormaVie : MonoBehaviour
{
    [SerializeField] private MainCharester _charecter;
    [SerializeField] private TMP_InputField _name;
    [SerializeField] private TMP_InputField _opisaniy;
    [SerializeField] private TMP_Dropdown _tipe;
    [SerializeField] private TMP_Dropdown _class;
    [SerializeField] private TextMeshProUGUI _status;

    private void OnEnable()
    {
        // �������� �� �������.
        MainCharester.OnFormaEvent += UbgrateFiles; // �������� ����� / �������
    }

    // ���� ���� += , �� ����� ������ ������� -=.

    private void OnDisable()
    {
        // ������� �� �������.
        MainCharester.OnFormaEvent -= UbgrateFiles; 
    }

    private void Start()
    {
        // enabled - �������� � ��������� ������� � ���������� �� ����� (���� ��� �� �������). 
        _status.enabled = !_status.enabled;
    }

    public void UbgrateFiles(/*string _nike, int _index, string classText, string newTipe*/)
    {
        string _nike = _name.text;//_name.text; // �����
        int _index = _class.value; // ��������
        string classText = _class.options[_index].text; // ����� �������� �����
        string newTipe = _tipe.options[_index].text;


        Debug.Log(_index);
        if (_nike == "")
        {
            _status.enabled = true;
            _status.text = $"������� ��� {_nike}";
        }
        else
        {
            // �������� �� �������.
            _charecter.OffForm();
        }
    }

    /*
    _name.text = "";
    _opisaniy.text = "";
    _tipe.value = 0;
    _class.value = 0;*/
}
